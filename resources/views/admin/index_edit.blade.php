@extends('layouts.admin')

@section('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>

    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'fontColor' ],
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                    ]
                }
            } )
            .catch( error => {
                console.log( error );
            } );
    </script>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.edit') }}
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route("admin.settings.index.update") }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <input class="form-control {{ $errors->has('header_title') ? 'is-invalid' : '' }}" value="{{ old('header_title', $index->header_title) }}" name="header_title" id="header_title">
                    @if($errors->has('header_title'))
                        <div class="invalid-feedback">
                            {{ $errors->first('header_title') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <input class="form-control {{ $errors->has('header_text') ? 'is-invalid' : '' }}" value="{{ old('header_text', $index->header_text) }}" name="header_text" id="header_text">
                    @if($errors->has('header_text'))
                        <div class="invalid-feedback">
                            {{ $errors->first('header_text') }}
                        </div>
                    @endif
                </div>


                <div class="form-group">
                    <textarea class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}" name="content" id="content" required>{{ old('content', $index->content) }}</textarea>
                    @if($errors->has('content'))
                        <div class="invalid-feedback">
                            {{ $errors->first('content') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>



@endsection
