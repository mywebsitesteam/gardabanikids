<section class="container-fluid p-0 m-0 w-100  header-section">
    <div class="row p-0 m-0 h-100 w-100 justify-content-center">
        <div class="col-12 header-section px-0 position-relative">
            <div class="header-section__wrapper h-100 position-relative  section-img"></div>
            <div class="position-absolute text-center section-text">
                <div class="header-title txt-graysh">
                    <p class="mb-2 mt-3 text-centered">{{ $index->header_title }}</p>
                </div>
                <div class="header-description txt-graysh">
                    <p class="mb-2 text-centered">{{ $index->header_text }}</p>
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <div onclick="Router('/check-if-registered')" class="cursor-pointer br-46 bg-dark-blue d-inline-block position-relative">
                        <p class="txt-graysh text-center px-5 py-3">გადაამოწმეთ ბავშვის რეგისტრაცია</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="d-flex flex-column justify-content-center mt-4">
        <div class="py-2">
            <div class="pt-3">
                {!! $index->content !!}
            </div>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center my-4">
    <div onclick="Router('/register')" class="cursor-pointer br-46 bg-dark-blue d-inline-block position-relative">
        <p class="txt-graysh px-5 py-3">დარეგისტრირდი</p>
    </div>
</div>
