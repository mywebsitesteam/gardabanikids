<div class="container center-vertically pt-106">
    <div class="row align-items-center">
        <div class="col-xl-3 col-lg-3 col-md-12 position-relative">
            <div>
                <div class="section-header mb-4">
                    <p class="mb-0">რეგისტრაცია</p>
                </div>
                <div class="section-desc mb-4">
{{--                    <p class="mb-0" style="color: #4d5a89">ნომერ 41 ფილიალში ჩასარიცხად ელექტრონულ რეგისტრაციაში მონაწილეობას იღებენ მხოლოდ ჭყონდიდელის დასახლებაში დარეგისტრირებული ახალგაზრდები.</p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 br-46 bg-dark-blue py-4 px-5">

            <div id="success" class="my-4" style="display: none">
                <p class="text-white">რეგისტრაცია წარმატებით განხორციელდა.</p>
                <p class="text-white">ბაღის ფილიალი: <span id="branch"></span></p>
                <p class="text-white">ფილიალის ჯგუფი: <span id="group"></span></p>
            </div>

            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-xs-12">
                    <div class="col-12">
                        <p class="mb-2 txt-graysh">მშობელი ან მეურვე</p>
                    </div>
                    <div class="col-12">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control br-23" id="parent_name" placeholder="name@example.com">
                                <label for="parent_name">სახ.გვარი</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control br-23" id="parent_id_number" placeholder="name@example.com">
                                <label for="parent_id_number">პირადი ნომერი</label>
                        </div>
                        <div class="form-floating mb-3">
                            <select class="form-select br-23" id="floatingSelectGuardian" aria-label="Floating label select example">
                                <option value="დედა">დედა</option>
                                <option value="მამა">მამა</option>
                                <option value="მეურვე">მეურვე</option>
                            </select>
                            <label for="floatingSelectGuardian">აირჩიეთ მეურვის ტიპი</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control br-23" id="phone" placeholder="name@example.com">
                                <label for="phone">ტელ.ნომერი</label>
                        </div>
                   {{--  <div class="form-floating mb-3">
                            <input type="email" class="form-control br-23" id="email" placeholder="name@example.com">
                                <label for="email">ელ-ფოსტა</label>
                        </div> --}}
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-xs-12">
                    <div class="col-12">
                        <p class="mb-2 txt-graysh">ბავშვი</p>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control br-23" id="name" placeholder="name@example.com">
                                <label for="name">სახ.გვარი</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control br-23" id="id_number" placeholder="name@example.com">
                                <label for="id_number">პირადი ნომერი</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control br-23" id="date_of_birth" placeholder="name@example.com">
                                <label for="date_of_birth">დაბადების თარიღი</label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-floating mb-3">
                                <select class="form-select br-23" id="floatingSelectCategories" aria-label="Floating label select example">
                                    <option selected>აირჩიეთ კატეგორია</option>
                                </select>
                                <label for="floatingSelectCategories">კატეგორია</label>
                            </div>
                            <div class="form-floating mb-3">
                                <select class="form-select br-23" id="floatingSelectNationalities" aria-label="Floating label select example">
                                    <option selected>აირჩიეთ ეროვნება</option>
                                </select>
                                <label for="floatingSelectCategories">ეროვნება</label>
                            </div>
                            <div class="form-floating mb-3">
                                <select class="form-select br-23" id="floatingSelectBranches" aria-label="Floating label select example">
                                    <option selected>აირჩიეთ ფილიალი</option>
                                </select>
                                <label for="floatingSelectBranches">ბაღის ფილიალი</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating mb-3">
                                <select class="form-select br-23" id="floatingSelectGroups" aria-label="Floating label select example">
                                </select>
                                <label for="floatingSelectGroups">ფილიალის ჯგუფები</label>
                            </div>
                            <div class="form-floating mb-3" style="display: none" id="floatingSelectAddonGroupDiv">
                                <select class="form-select br-23" id="floatingSelectAddonGroups" aria-label="Floating label select example">
                                    <option selected>დამატებითი ჯგუფი</option>
                                </select>
                                <label for="floatingSelectGroups">დამატებითი ჯგუფი</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="text-center bg-greysh br-23 cursor-pointer">
                        <p
                        id="form-submit" type="submit" class="mb-0 d-inline-block txt-bluish w-100 py-3 cursor-pointer">გაგრძელება</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function isEmpty(obj) {
        for(let prop in obj) {
            if(prop !== 'addon_group_id' && (obj[prop] !== null || obj[prop] == '')){
                return true;
            }
            console.log(obj[prop])
        }

        return false;
    }

    //=====================რეგისტრაციის გამორთვა=====================
    $("#form-submit").on('click', function() {
        alert('რეგისტრაცია დროებით შეჩერებულია')
    });

    //=================Register Button Click Event==========================
    //გამოვრთე დროებით

    // $("#form-submit").on('click', function() {

    //     alert("რეგისტრაცია დროებით შეჩერებულია");

    //     const kidFullname = fullNameSplit($("#name").val());
    //     console.log(kidFullname)
    //     // console.log(kidFullname);
    //     const parentFullname = fullNameSplit($("#parent_name").val());
    //     let data = {
    //         'parent_id_number' : $("#parent_id_number").val(),
    //         'guardian_type' : $("#floatingSelectGuardian").find(':selected').val(),
    //         'phone' : $("#phone").val(),
    //         //'email' : $("#email").val(),
    //         'parent_name' : parentFullname.name,
    //         'parent_lastname' : parentFullname.lastname,
    //         'id_number' : $("#id_number").val(),
    //         'name' : kidFullname.name,
    //         'lastname' : kidFullname.lastname,
    //         'date_of_birth' : $("#date_of_birth").val(),
    //         'branch_id' : $("#floatingSelectBranches").find(':selected').val(),
    //         'kid_category_id' : $("#floatingSelectCategories").find(':selected').val(),
    //         'kid_nationality_id' : $("#floatingSelectNationalities").find(':selected').val(),
    //         'group_id' : $("#floatingSelectGroups").find(':selected').val(),
    //         'addon_group_id' : $("#floatingSelectAddonGroups").find(':selected').val(),
    //         "_token": "{{ csrf_token() }}"
    //     }

    //     //if input or select is empty, send error message
    //     if(isEmpty(data)){
    //         alert('შეავსეთ ყველა ველი');
    //         return;
    //     }

    //     console.log(data)

    //     // if(isEmpty(data)){
    //     //     return alert('გთხოვთ შეავსეთ ყველა ველი!');
    //     // }

    //     $.ajax({
    //         url: "{{ route('register') }}",
    //         method: "POST",
    //         data: data,
    //         dataType: "json",
    //         success: function(res){
    //            if(res.type == 'success'){
    //                if(res.message == null || res.message == ""){
    //                    $('#success')
    //                        .find('#branch').html($("#floatingSelectBranches").find(':selected').html())
    //                    $('#success')
    //                        .find('#group').html($("#floatingSelectGroups").find(':selected').html())
    //                    $('#success')
    //                        .show();
    //                }else{
    //                    $('#success')
    //                        .html(`<p class="text-white">${res.message}</p>`)
    //                    $('#success')
    //                        .show();
    //                }
    //            }
    //         },
    //         error: function(err){
    //            alert(JSON.stringify(err));
    //         }
    //     });
    // });




    $("#floatingSelectBranches").change(() => {
        const selectedBranchId = $("#floatingSelectBranches").find(":selected").val();
          let data = {
            'id' : selectedBranchId,
            "_token": "{{ csrf_token() }}"
        }
        // console.log(data);
        $.ajax({
            url: "{{ route('getBranchGroups') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                // const arr = Object.entries(res);
                $('#floatingSelectAddonGroups').empty();
                $('#floatingSelectAddonGroupDiv').hide();
                $('#floatingSelectGroups').empty();

                res.forEach((element, index) => {
                    if(index == 0 && element.vacancy == 0) getGroups(selectedBranchId, element.id);

                    $('#floatingSelectGroups').append(`<option data-vacancy="${element.vacancy}" value="${element.id}">${element.name}</option>`)
                });
            },
            error: function(err){
               alert(JSON.stringify(err));
            }
        });
    });

    $("#floatingSelectGroups").change(() => {
        console.log('Change')

        let vacancy = $("#floatingSelectGroups").find(':selected').data('vacancy');
        let groupId = $("#floatingSelectGroups").find(':selected').val();
        console.log(vacancy)

        const selectedBranchId = $("#floatingSelectBranches").find(":selected").val();

        if(vacancy == 0) {
            getGroups(selectedBranchId, groupId)
        }else{
            $('#floatingSelectAddonGroups').empty();
            $('#floatingSelectAddonGroupDiv').hide()
        }
    });

    function getGroups(selectedBranchId, groupId = null) {
        let data = {
            'id' : selectedBranchId,
            'expect': groupId,
            "_token": "{{ csrf_token() }}"
        }

        $.ajax({
            url: "{{ route('getBranchGroups') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                $('#floatingSelectAddonGroups').empty();

                res.forEach(element => {
                    $('#floatingSelectAddonGroups').append(`<option value="${element.id}">${element.name}, ${element.kindergarden_branch.name ?? ''}</option>`)
                });

                $('#floatingSelectAddonGroupDiv').show()
            },
            error: function(err){
                alert(JSON.stringify(err));
            }
        });
    }

    $(document).ready(function(){
          let data = {
            'page' : 0,
            "_token": "{{ csrf_token() }}"
        }
      //   console.log(data);
        $.ajax({
            url: "{{ route('getBranches') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                const arr = Object.entries(res);

                // branches = arr.map((el, index) => {
                //     return {
                //         'id': el[1],
                //         'name': el[0]
                //     }
                // });

                arr.forEach((element, index) => {
                    $('#floatingSelectBranches').append(`<option value="${element[1]}">${element[0]}</option>`)
                });
            },
            error: function(err){
               alert(JSON.stringify(err));
            }
        });

        $.ajax({
            url: "{{ route('getKidCategories') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                const arr = Object.entries(res);
                arr.forEach(element => {
                    $('#floatingSelectCategories').append(`<option value="${element[1]}">${element[0]}</option>`)
                });
            },
            error: function(err){
                alert(JSON.stringify(err));
            }
        });


        $.ajax({
            url: "{{ route('getKidNationalities') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                const arr = Object.entries(res);
                arr.forEach(element => {
                    $('#floatingSelectNationalities').append(`<option value="${element[1]}">${element[0]}</option>`)
                });
            },
            error: function(err){
                alert(JSON.stringify(err));
            }
        });

        $("#date_of_birth").datepicker({ dateFormat: 'yy-mm-dd' });

    });

    function fullNameSplit(fullName)
    {
        // console.log(fullName);
        const name = fullName.split(' ')[0];
        const lastname = fullName.split(' ')[1];

        return {'name': name,'lastname': lastname}
    }

    if($(window).width() <= 991){
        $("#removeAbsoluteOnResponsive").removeClass();
    }
    jQuery( document ).ready(function() {
        jQuery('.col-xl-9.col-lg-9.col-md-12.br-46.bg-dark-blue.py-4.px-5 input').each(function(k,v){
            jQuery(v).geokbd();
        })
    })
</script>
