<div class="container center-vertically pt-106">
    <div class="row align-items-center">
        <div class="col-xl-4 col-lg-4 col-md-12 position-relative">
            <div>
                <div class="section-header mb-4">
                    <p class="mb-0">დაგვიკავშირდით</p>
                </div>
                <div class="section-desc mb-4">
{{--                    <p class="mb-0">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, ex.</p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 br-46 bg-dark-blue py-4 px-5">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 mb-3">
                    <div class="col-12">
                        <p class="mb-2 txt-graysh">კონტაქტი</p>
                    </div>
                    <div class="col-12 bg-white br-23 py-4 px-3">
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/phone-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4">595057352</p>
                        </div>
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/person-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4">მანოლა გველებიანი</p>
                        </div>
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/phone-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4">595057350</p>
                        </div>
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/person-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4">გულიკო ავალიანი</p>
                        </div>
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/address-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4">გარდაბანი, დავით აღმაშენებლის  127</p>
                        </div>
                        <div class="info-single d-flex justify-content-start pb-1 px-2 mb-3">
                            <img src="{{ asset('assets/img/web-icon.svg') }}" alt="">
                            <p class="mb-0 ms-4" style="overflow-wrap: anywhere;">gvelebianimanola@gmail.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 mb-3">
                    <div class="col-12">
                        <p class="mb-2 txt-graysh">მოგვწერეთ</p>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control br-23" id="name" placeholder="name@example.com">
                                        <label for="name">სახელი</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control br-23" id="phone" placeholder="name@example.com">
                                        <label for="phone">ტელ.ნომერი</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control br-23" id="lastname" placeholder="name@example.com">
                                        <label for="lastname">გვარი</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control br-23" id="email" placeholder="name@example.com">
                                        <label for="email">ელ.ფოსტა</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating mb-3">
                                <textarea class="form-control br-23" placeholder="Leave a comment here" id="text" style="height: 195px;"></textarea>
                                <label for="text">ტექსტი</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12">
                    <div class="text-center bg-greysh br-23 cursor-pointer">
                        <p id="form-submit" type="submit" class="mb-0 d-inline-block txt-bluish w-100 py-3 cursor-pointer">გაგზავნა</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function isEmpty(obj) {
        for(let prop in obj) {
            if(obj[prop] == null || obj[prop] == ''){
                return true;
            }
        }

        return false;
    }


    $('#form-submit').on('click', function (e){
        e.preventDefault();

        let data = {
            'phone' : $("#phone").val(),
            'email' : $("#email").val(),
            'name' : $("#name").val(),
            'lastname' : $("#lastname").val(),
            'text' : $("#text").val(),
            "_token": "{{ csrf_token() }}"
        }

        if(isEmpty(data)){
            return alert('გთხოვთ შეავსეთ ყველა ველი!');
        }

        $.ajax({
            url: "{{ route('contact.send') }}",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(res){
                if(res.type == 'success'){
                    Router();
                    alert('მეილი წარმატებით გაიგზავნა!')
                }
            },
            error: function(err){
                alert(JSON.stringify(err));
            }
        });
    })

    if($(window).width() <= 991){
        $("#removeAbsoluteOnResponsive").removeClass();
    }
</script>
