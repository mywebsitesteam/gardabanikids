<?php

namespace App\Exports;

use App\Models\Kid;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class KidsByGroup  implements FromCollection, WithColumnWidths,WithHeadings
{

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 25,
            'B' => 25,
            'C' => 25,
            'D' => 25,
            'E' => 25,
            'F' => 25,
            'G' => 25,
            'H' => 25,
            'I' => 25,
            'J' => 25,
//            'K' => 20,
//            'L' => 20,
//            'M' => 20,
//            'N' => 20,
//            'O' => 20,
//            'P' => 20,
//            'Q' => 20,
//            'R' => 20,
//            'S' => 20,
//            'T' => 20,
//            'U' => 20
        ];
    }

    public function headings(): array
    {
        return [
            'სახელი  გვარი',
            'პირადი №',
            'დაბადების თარიღი',
            'ბაღის ფილიალი',
            'ჯგუფი',
            'კატეგორია',
            'მშობლის სახელი გვარი',
            'მშობლის პირადი №',
            'მშობლის ნომერი'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_DATE_DMYMINUS,
//            'I' => NumberFormat::FORMAT_NUMBER,
//            'K' => NumberFormat::FORMAT_NUMBER,
//            'L' => NumberFormat::FORMAT_NUMBER
        ];
    }

    public function collection()
    {
        $kids = Kid::query()->where('group_id', '=',  $this->id)->get();
        if (count($kids) == 0){
            $kids = Kid::all();
        }
        return $kids->map(function ($kidsRelation) {
            $parent_lastname = $kidsRelation->parent_guardian->lastname ?? null;
            $parent_name = $kidsRelation->parent_guardian->name ?? null;
            $kidName = $kidsRelation->name ?? null;
            $kidLastname = $kidsRelation->lastname ?? null;
            return [
                'kid_name' => sprintf("%s %s", $kidName,$kidLastname),
                'id_number' => @$kidsRelation->id_number .' ' ?? " ",
                'date_of_birth' => @$kidsRelation->date_of_birth ?? " ",
                'kindergarden_name' => @$kidsRelation->branch->name ?? " ".' ',
                'group' => @$kidsRelation->group->name ?? " ".' ',
                'category' => @$kidsRelation->category->name ?? " ".' ',
                'parent_name' =>  sprintf("%s %s", $parent_name, $parent_lastname),
                'parent_id_number' => @$kidsRelation->parent_guardian->id_number.' ' ?? null,
                'parent_phone' => @$kidsRelation->parent_guardian->phone.' ' ?? null
            ];
        });
    }

}
