<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use App\Models\ReceivedMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function send(Request $request){

        $data = $this->validate($request,[
            'name' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required',
            'text' => 'required'
        ]);

        ReceivedMail::create([
            'name'              => $data['name'],
            'lastname'          => $data['lastname'],
            'email'             => $data['email'],
            'phone_number'      => $data['phone'],
            'content'           => $data['text']
        ]);

//        Mail::to('tom@tom.com')->send(new ContactForm($data));

        return \Response::json([
            'type' => 'success'
        ]);
     }
}
