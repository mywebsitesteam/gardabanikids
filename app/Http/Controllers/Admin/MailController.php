<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReceivedMail;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('mail_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mails = ReceivedMail::get();

        return view('admin.recivedMail.index', compact('mails'));
    }

    public function show(ReceivedMail $receivedMail)
    {
        abort_if(Gate::denies('mail_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.recivedMail.show')
            ->with('mail', $receivedMail);
    }
}
