<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Index;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{
    public function index_edit()
    {
        abort_if(Gate::denies('index_text_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $index = Index::first();

        if(!$index){
            $index = Index::create([
                'content' => 'ტექსტი',

            ]);
        }

        return view('admin.index_edit', compact('index'));
    }

    public function index_update(Request $request)
    {
        abort_if(Gate::denies('index_text_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');


        $index = Index::first();

        $index->update([
            'content' => $request->post('content'),
            'header_title' => $request->post('header_title'),
            'header_text' => $request->post('header_text')
        ]);

        return redirect()->back();
    }
}
