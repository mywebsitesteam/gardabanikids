<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kid_categories')->insert(
            [
                'name' => 'სოციალურად დაუცველი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'მრავალშვილიანი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'ობოლი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'შეზღუდული შესაძლებლობის',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'სპეციალურად საჭიროების',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'აღსაზრდელი რომლის დედმამიშვილიც დადის ბაღში',
                'user_id' => 1,
            ]
        );

        DB::table('kid_categories')->insert(
            [
                'name' => 'სასკოლე ასაკი',
                'user_id' => 1,
            ]
        );
    }
}
