<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class NationalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kid_nationalities')->insert(
            [
                'name' => 'ქართველი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_nationalities')->insert(
            [
                'name' => 'აზერბაიჯანელი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_nationalities')->insert(
            [
                'name' => 'ობოლი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_nationalities')->insert(
            [
                'name' => 'სომეხი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_nationalities')->insert(
            [
                'name' => 'ასირიელი',
                'user_id' => 1,
            ]
        );

        DB::table('kid_nationalities')->insert(
            [
                'name' => 'სხვა',
                'user_id' => 1,
            ]
        );
    }
}
